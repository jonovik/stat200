#' Exercise 68, "Caring for hospital patients", in Chapter 3 of Mendenhall & Sincich 2014.
#'
#' Data on fifty heart patients admitted to Bayonet Point Hostpital (St Petersburg, Florida).
#'
#' In medical jargon, any item used a patient's care is called a "factor".
#' (Not to be confused with the `factor` data type in R!)
#'
#' The number of "factors" used for each factor and their length of stay is recorded.
#'
#' @format A data frame with 50 observations (one row per patient) on the following 2 variables:
#' \describe{
#'   \item{x}{Integer: Number of medical tems used in the care of the patient.}
#'   \item{y}{Integer: Length of hospital stay.}
#' }
#' @source Mendenhall & Sincich (2014). Regression Analysis, 7th ed.
"FACTORS"
