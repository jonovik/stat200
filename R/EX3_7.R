#' Chapter 3 Exercise 7 Data
#'
#' Toy dataset for Exercise 7 in Chapter 3 of Mendenhall and Sincich (2014) Regression analysis, 7th ed.
#'
#' @format A data frame with 5 observations on the following 2 variables:
#' \describe{
#'   \item{x}{A numeric vector.}
#'   \item{y}{A numeric vector, response variable.}
#' }
"EX3_7"
