#' Carp diet study
#'
#' Data for Chapter 4 Exercise 37 of Mendenhall and Sincich (2014) Regression analysis, 7th ed.
#'
#' Carp were fed a protein-free diet three times daily for a period of 20 days.
#' Tanks held between 2 and 15 fish each, but unfortunately we have no data on how many.
#' It seems there is no control diet group in the dataset.
#'
#' @format A data frame with 10 observations on the following 3 variables:
#' \describe{
#'   \item{TANK}{Tank id (integer).}
#'   \item{WEIGHT}{Mean body weight (grams).}
#'   \item{ENE}{Endogenous nitrogen excretion (mg per 100 g body weight per day).}
#' }
#' @source [Source of the data]
#' Watanabe T, Ohta M (1995) Endogenous nitrogen excretion an non-fecal energy losses in carp and rainbow trout.
#' *Fisheries Science* 61:56 (Table 5).
"CARP"
