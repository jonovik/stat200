#' Data Exam EDS220 2012 Example 5
#'
#' Reduction in light intensity after 1000 vs 100 hours of use for nine brands of light bulb.
#'
#' @format A data frame with 9 observations on the following 3 variables:
#' \describe{
#'   \item{i}{Integer: index of observation}
#'   \item{price}{Integer: Price in Norwegian kroner}
#'   \item{reduction}{Character: Percent reduction in light intensity after 1000 vs 100 hours of use}
#' }
"DataExamEDS220_2012_5"
