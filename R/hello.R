# Scrapbook
if (FALSE) {
  library(tidyverse)
  dir(path = "c:/git/teach/STAT200/R/data", pattern = "[Rr][Dd]ata", recursive = TRUE, full.names = TRUE) |> clipr::write_clip()
  dir(path = "c:/git/teach/STAT200/Exercises", pattern = "[Rr][Dd]ata", recursive = TRUE, full.names = TRUE)
  intersect(dir(path = "c:/git/teach/STAT200/R/data", pattern = "[Rr][Dd]ata", recursive = TRUE),
            dir(path = "c:/git/teach/STAT200/Exercises", pattern = "[Rr][Dd]ata", recursive = TRUE))
  # Equal: Bear_full, snordis2, vacuum3
  dir(path = "c:/git/teach/STAT200/R/data", pattern = "[Rr][Dd]ata", recursive = TRUE) |> clipr::write_clip()
  # d Bear_full.RData
  #   Bear_WeightNeck.RData
  #   Bear2.RData
  # b boiler_474.Rdata
  #   crabs.RData
  # b DISCRIM.Rdata
  #   ex2.RData
  #   ex3.RData
  # b ftccigar.RData
  # b hospital.RData
  # b orange.RData
  #   phos.RData
  #   pigs.RData
  #   poly.RData
  #   problem720.RData
  #   slaughter.RData
  # d snordis2.RData
  # b spruce.RData
  #   spruce_e3.RData
  #   Stroke.RData
  #   vacuum3.RData
  #   volum.RData
  #   Yatzy.RData
  #   ZincPH.RData
  dir(path = "c:/git/teach/STAT200/Exercises", pattern = "[Rr][Dd]ata", recursive = TRUE) |> clipr::write_clip()
  # d Bear_full.RData
  # b BLACKBREAM.RData
  # d BOILERS.RData
  #   CARP.RData
  #   DataExamEDS220_2012_5.RData
  #   EX3_6.RData
  #   EX3_7.RData
  #   EX7_20.RData
  #   FACTORS.RData
  # b FTCCIGAR.RData
  # b OJUICE.RData
  # d snordis2.RData
  #   STREETVEN.RData
  # d vacuum3.RData
  #   WHITESPRUCE.RData

  local({load("c:/git/teach/STAT200/R/data/Bear_full.RData"); Bear_full})
  BIAS.data::bears |>
    select(Weight, Length, Age, Month, HL = Head.L, HW = Head.W, Neck = Neck.G, Chest = Chest.G, Gender = Sex) |>
    compare::compare(local({load("c:/git/teach/STAT200/R/data/Bear_full.RData"); Bear_full}))  # TRUE
  # --> Rename in all exercise scripts and slides:
  # HL --> Head.L, HW --> Head.W, Neck --> Neck.G, Chest --> Chest.G, Gender --> Sex
  compare::compare(local({load("c:/git/teach/STAT200/R/data/Bear_full.RData"); Bear_full}),
                   local({load("c:/git/teach/STAT200/Exercises/Bear_full.RData"); Bear_full}))  # TRUE

  local({load("c:/git/teach/STAT200/R/data/boiler_474.Rdata"); boilers}) |>
    compare::compare(BIAS.data::boilers)  # TRUE

  local({load("c:/git/teach/STAT200/R/data/DISCRIM.Rdata"); DISCRIM}) |>
    compare::compare(BIAS.data::DISCRIM)  # TRUE

  local({load("c:/git/teach/STAT200/R/data/ftccigar.RData"); ftccigar}) |>
    compare::compare(BIAS.data::ftccigar)  # TRUE

  local({load("c:/git/teach/STAT200/R/data/hospital.RData"); hospital}) |>
    compare::compare(BIAS.data::hospital, allowAll = TRUE)  # TRUE
  # --> Rename in all exercise scripts and slides: x --> Num_factors, y --> Stay_length

  local({load("c:/git/teach/STAT200/R/data/orange.RData"); orange}) |>  # Run SwI Pectin
    compare::compare(BIAS.data::OrangeJuice, allowAll = TRUE)  # TRUE
  # --> Explain in Rmd that OrangeJuice is OJUICE in textbook

  local({load("c:/git/teach/STAT200/R/data/spruce.RData"); spruce}) |>  # Run SwI Pectin
    compare::compare(BIAS.data::spruce, allowAll = TRUE)  # TRUE

  local({load("c:/git/teach/STAT200/Exercises/BLACKBREAM.RData"); BLACKBREAM}) |>
    compare::compare(BIAS.data::Blackbream)  # TRUE

  # Bear_WeightNeck.RData
  local({load("c:/git/teach/STAT200/R/data/Bear_WeightNeck.RData"); Bear}) |>
    compare::compare(BIAS.data::bears |> select(Row = ID, Weight, Neck = Neck.G))  # TRUE
  # Change in Rmd (possibly never used) and remove from Canvas/repo.

  # Bear2.RData
  BIAS.data::bears |>
    select(Weight, Length, Age, Month, HL = Head.L, HW = Head.W, Neck = Neck.G, Chest = Chest.G, Gender = Sex) |>
    compare::compare(local({load("c:/git/teach/STAT200/R/data/Bear2.RData"); Bear2}), allowAll = TRUE)  # TRUE
  # Gender is character in Bear2 and factor in bears, but alpha order. Change in Rmd and remove from Canvas/repo.

  # crabs.RData
  local({load("c:/git/teach/STAT200/R/data/crabs.RData"); crabs}) |>
    compare::compare(BIAS.data::crabs, allowAll = TRUE)  # TRUE
  local({load("c:/git/teach/STAT200/Exam_Jan_2023/crab_stat200.RData"); crabs}) |>
    compare::compare(local({load("c:/git/teach/STAT200/R/data/crabs.RData"); crabs}))  # TRUE
  BIAS.data::crabs |>
    transmute(width, weight, colour = color, satellites = num.satellites, colour.recoded = as.integer(color))
  # Length was simulated as 1.3*width + noise
  # # # From STAT340 2019, https://github.com/thoree/BIAS.data
  # fname <- "crab_stat200.RData"
  # --> Change to use BIAS.data and delete the other one

  # All-caps names are from Mendenhall & Cincich. Keep them if in library(stat200), and explain correspondence with library(BIAS.data) if they exist there with a more descriptive name.

  # ex2.RData used in R\ch7_examples.Rmd and chapter7.pptx chapter7.pdf
  local({load("c:/git/teach/STAT200/R/data/ex2.RData"); ex2})
  # Put throwaway code in library(stat200) but give more context in name:
  # ex2.RData --> stat200::ch7_ex2
  # New values are defined in ch7_examples.Rmd
  # Could perhaps add new x2 as x3, pointing out that they are nearly equal to x2.

  # ex3.RData used in R\ch7_examples.Rmd and chapter7.pptx chapter7.pdf
  local({load("c:/git/teach/STAT200/R/data/ex3.RData"); ex3})
  # ex3.RData --> stat200::ch7_ex3

  # phos.RData used in R\ch4_examples.Rmd and chapter4.pptx chapter4.pdf
  local({load("c:/git/teach/STAT200/R/data/phos.RData"); phos})
  # phos.RData --> stat200::ch4_phos

  #   pigs.RData used in R\ch5_examples.Rmd and chapter5.pptx chapter5.pdf
  local({load("c:/git/teach/STAT200/R/data/pigs.RData"); pigs})
  # pigs.RData --> stat200::ch5_pigs

  #   poly.RData used in R\ch4_examples.Rmd and chapter4.pptx chapter4.pdf
  local({load("c:/git/teach/STAT200/R/data/poly.RData"); poly})
  # poly.RData --> stat200::ch4_poly

  #   problem720.RData used in R\ch7_examples.Rmd and chapter7.pptx chapter7.pdf
  local({load("c:/git/teach/STAT200/R/data/problem720.RData"); problem720})
  # problem720.RData --> stat200::ch7_problem720

  #   slaughter.RData used in R\ch7_examples.Rmd and chapter7.pptx chapter7.pdf
  local({load("c:/git/teach/STAT200/R/data/slaughter.RData"); slaughter})
  # slaughter.RData --> stat200::ch7_slaughter

  #   snordis2.RData used in R\ch9_examples.Rmd and chapter9.pptx chapter9.pdf and Exer4cises12_ch9.6.pdf
  local({load("c:/git/teach/STAT200/R/data/snordis2.RData"); snordis2})
  # snordis2.RData --> stat200::snordis2

  #   spruce_e3.RData used in ch3_examples.Rmd and chapter3.pptx chapter3.pdf
  local({load("c:/git/teach/STAT200/R/data/spruce_e3.RData"); spruce_e3})
  # spruce_e3.RData --> stat200::spruce_e3

  #   Stroke.RData used in ch3_examples.Rmd and chapter3.pptx chapter3.pdf
  local({load("c:/git/teach/STAT200/R/data/Stroke.RData"); Stroke})
  # Stroke.RData --> stat200::Stroke

  #   vacuum3.RData used in ch5_exercises.Rmd (Exam problem 2 2012) now in Exercises8_ch6.Rmd
  local({load("c:/git/teach/STAT200/R/data/vacuum3.RData"); vacuum})
  # vacuum3.RData/vacuum --> stat200::vacuum

  #   volum.RData used in ch5_examples.Rmd and chapter5.pptx chapter5.pdf
  local({load("c:/git/teach/STAT200/R/data/volum.RData"); volum})
  # volum.RData --> stat200::spruce_e3
  #   Same as spruce_e3.RData used in ch3_examples.Rmd and chapter3.pptx chapter3.pdf
  local({load("c:/git/teach/STAT200/R/data/spruce_e3.RData"); spruce_e3})

  local({load("c:/git/teach/STAT200/R/data/volum.RData"); volum}) |>
    select(-a) |>
    compare::compare(local({load("c:/git/teach/STAT200/R/data/spruce_e3.RData"); spruce_e3}))  # TRUE

  #   Yatzy.RData used in ch3_examples.Rmd and chapter3.pptx chapter3.pdf
  local({load("c:/git/teach/STAT200/R/data/Yatzy.RData"); Yatzy})
  # Yatzy.RData --> stat200::Yatzy

  #   ZincPH.RData used in ch3_examples.Rmd and chapter3.pptx chapter3.pdf
  local({load("c:/git/teach/STAT200/R/data/ZincPH.RData"); ZincPH})
  # ZincPH.RData --> stat200::ZincPH


  #   CARP.RData used in Exercises6_ch4_ch5.Rmd
  local({load("c:/git/teach/STAT200/Exercises/CARP.RData"); CARP})
  # CARP.RData --> stat200::CARP

  #   DataExamEDS220_2012_5.RData used in Excercises1_ch3.Rmd
  local({load("c:/git/teach/STAT200/Exercises/DataExamEDS220_2012_5.RData"); DataExamEDS220_2012_5})
  # DataExamEDS220_2012_5.RData --> stat200::DataExamEDS220_2012_5

  #   EX3_6.RData
  local({load("c:/git/teach/STAT200/Exercises/EX3_6.RData"); EX3_6})
  # Not sure this was ever used. x = 1:6, y = c(1, 2, 2, 3, 5, 5)

  #   EX3_7.RData
  local({load("c:/git/teach/STAT200/Exercises/EX3_7.RData"); EX3_7})
  # Not sure this was ever used. x = -2:2, y = c(4, 3, 3, 1, -1)

  #   EX7_20.RData used in Exercises9_ch7.Rmd
  local({load("c:/git/teach/STAT200/Exercises/EX7_20.RData"); EX7_20})
  # EX7_20.RData --> stat200::EX7_20

  #   FACTORS.RData used in Exercises3_ch3.Rmd
  local({load("c:/git/teach/STAT200/Exercises/FACTORS.RData"); FACTORS})
  # FACTORS.RData --> stat200::FACTORS

  #   FTCCIGAR.RData used in Excercises9_ch7.Rmd
  local({load("c:/git/teach/STAT200/Exercises/FTCCIGAR.RData"); FTCCIGAR}) |>
    compare::compare(BIAS.data::ftccigar)  # TRUE

  #   OJUICE.RData possibly appears in ch3_exercises where pasted from clipboard (sigh). Possibly reworked by Nikolai, see Ch3 pdf

  #   STREETVEN.RData used in Exercises4_ch4.Rmd
  local({load("c:/git/teach/STAT200/Exercises/STREETVEN.RData"); STREETVEN})
  # STREETVEN.RData --> stat200::STREETVEN and perhaps eventuall BIAS.data

  #   WHITESPRUCE.RData used in Exercises2_ch3.Rmd
  local({load("c:/git/teach/STAT200/Exercises/WHITESPRUCE.RData"); WHITESPRUCE})
  # WHITESPRUCE.RData --> stat200::WHITESPRUCE

}
