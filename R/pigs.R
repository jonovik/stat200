#' Pig Diets and Weight Increase: Example 3 from lecture on Chapter 5 of Mendenhall & Sincich 2014
#'
#' This study examined the effects of two different types of diets on the weight increase of pigs over a feeding trial.
#'
#' @format A data frame with 16 observations on the following 3 variables:
#' \describe{
#'   \item{Y}{Numeric: Weight increase from the start until the end of the period (kg).}
#'   \item{X1}{Numeric: Weight of the pig when the trial started (kg).}
#'   \item{X2}{Integer: Kind of feed, an indicator variable where 0 represents one type of feed and 1 represents another.}
#' }
#' @source "Mendenhall & Sincich (2014) Regression analysis, 7th ed."
"pigs"
