#' Yatzy: Labrador puppy growth
#'
#' Yatzy is the companion of a former course responsible for STAT200.
#' This dataset documents his growth over the first 35 weeks of his life.
#'
#' @format A data frame with 23 observations on the following 3 variables:
#' \describe{
#'   \item{days}{Yatzy's age in days.}
#'   \item{weeks}{Yatzy's age in weeks.}
#'   \item{weight}{Yatzy's weight in kg.}
#' }
"Yatzy"
