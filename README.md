---
title: STAT200 datasets
---

# Datasets for [STAT200](https://www.nmbu.no/course/STAT200) regression at NMBU

To follow along with the examples and exercises in the course, you can install `library(stat200)` as follows:

```r
# If necessary, first:
# install.packages("remotes")
remotes::install_gitlab("jonovik/stat200")
remotes::install_github("thoree/BIAS.data")
```

The last command is necessary because some STAT200 exercises use datasets from library(BIAS.data).
